package dim.kompo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dim.kompo.dao.UserRepository;
import dim.kompo.dto.JwtAuthenticationResponse;
import dim.kompo.dto.UserDto;
import dim.kompo.entity.Role;
import dim.kompo.entity.User;
import dim.kompo.security.config.JwtTokenProvider;

@RestController
@RequestMapping("/authorization")
public class AuthController {

	private static org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AuthController.class);

	private final AuthenticationManager authenticationManager;

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final JwtTokenProvider tokenProvider;

	public AuthController(final AuthenticationManager authenticationManager, final UserRepository userRepository,
			final PasswordEncoder passwordEncoder, final JwtTokenProvider tokenProvider) {
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.tokenProvider = tokenProvider;
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserDto userDto) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						userDto.getEmail(),
						userDto.getPassword()
				)
		);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserDto userDto) {
		if (userRepository.existsByUsername(userDto.getUsername())) {

			return ResponseEntity.badRequest().body("Username is already taken!");
		}

		if (userRepository.existsByEmail(userDto.getEmail())) {
			return ResponseEntity.badRequest().body("Email Address already in use!");
		}
		User user = new User();
		BeanUtils.copyProperties(userDto, user);

		user.setName(userDto.getEmail());
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		user.setRole(Role.ROLE_USER);

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		return ResponseEntity.created(location).body("User registered successfully");
	}
}
